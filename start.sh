#!/bin/bash

set -eu

_generateconfig() {
  sudo -u www-data --preserve-env php <<'EOF'
<?php
      require_once '/app/data/config/local.php';
      $runtime_config = array(
        'db_driver' => 'pdo_mysql',
        'db_host' => getenv('CLOUDRON_MYSQL_HOST'),
        'db_table_prefix' => null,
        'db_port' => getenv('CLOUDRON_MYSQL_PORT'),
        'db_name' => getenv('CLOUDRON_MYSQL_DATABASE'),
        'db_user' => getenv('CLOUDRON_MYSQL_USERNAME'),
        'db_password' => getenv('CLOUDRON_MYSQL_PASSWORD'),
        'mailer_spool_type' => 'memory',
        'mailer_spool_path' => '%kernel.root_dir%/../var/spool',
        'site_url' => getenv('CLOUDRON_APP_ORIGIN'),
        'trusted_proxies' => array(
          "0" => getenv('CLOUDRON_PROXY_IP')
        )
      );

      $parameters = array_replace($parameters, $runtime_config);
      unset($parameters['cache_path']);
      unset($parameters['log_path']);
      unset($parameters['image_path']);
      unset($parameters['tmp_path']);
      unset($parameters['api_rate_limiter_cache']); // https://docs.mautic.org/en/mautic-3-upgrade/upgrade-steps

      if (getenv('CLOUDRON_MAIL_SMTP_SERVER') != "") {
        $smtp_config = array(
          'mailer_from_email' => getenv('CLOUDRON_MAIL_FROM'),
          'mailer_from_name' => getenv('CLOUDRON_MAIL_FROM_DISPLAY_NAME') ?: 'Mautic',
          'mailer_dsn' => 'smtp://' . getenv('CLOUDRON_MAIL_SMTP_USERNAME') . ':' . getenv('CLOUDRON_MAIL_SMTP_PASSWORD') . '@' . getenv('CLOUDRON_MAIL_SMTP_SERVER') . ':'  . getenv('CLOUDRON_MAIL_SMTP_PORT'),
        );

        $parameters = array_replace($parameters, $smtp_config);
      }

      file_put_contents("/app/data/config/local.php", "<?php\n\$parameters = " . var_export($parameters, true) . ";\n");
EOF
}

echo "==> Creating directories"
mkdir -p /run/cron /run/mautic/{sessions,var/cache/run}
mkdir -p /app/data/{config,translations,media/files,media/images,themes,plugins,apache}
rm -rf /run/mautic/var/cache && cp -r /app/pkg/var/* /run/mautic/var    # ensure the directories

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

echo "==> Updating themes"
for theme in `find "/app/code/themes.orig"/* -maxdepth 0 -type d -printf "%f\n"`; do
    rm -rf "/app/data/themes/${theme}"
    cp -rf "/app/code/themes.orig/${theme}" /app/data/themes
done

echo "==> Updating plugins"
for plugin in `find "/app/code/plugins.orig"/* -maxdepth 0 -type d -printf "%f\n"`; do
    echo -e "\t ${plugin}"
    rm -rf "/app/data/plugins/${plugin}"
    cp -rf "/app/code/plugins.orig/${plugin}" /app/data/plugins
done

rm -rf /app/data/plugins/MauticLdapAuthBundle/ /app/data/plugins/MauticCitrixBundle

[[ ! -f /app/data/crontab.system ]] && cp /app/pkg/crontab.system.template /app/data/crontab.system

# this always overwrites with upstream files
cp -rf /app/code/media/images.orig/. /app/data/media/images/
cp -rf /app/code/media/files.orig/. /app/data/media/files/

# configure in-container Crontab - http://www.gsp.com/cgi-bin/man.cgi?section=5&topic=crontab
# we run as root user so that the cron tasks can redirect properly to the cron's stdout/stderr
# crontab does not like unqoted empty vars like FOO=, so the sed quotes it
if ! (env | sed -e 's,^\(.*\)=$,\1="",g'; cat /app/data/crontab.system; echo -e '\nMAILTO=""') | crontab -u root -; then
    echo "==> Error importing crontab. Continuing anyway"
else
    echo "==> Imported crontab"
fi

# ensure permissions for setup
chown -R www-data.www-data /app/data /run/mautic

if [[ ! -f /app/data/config/local.php ]]; then
    echo "==> Detected first run, creating config"
    echo "==> Installing Mautic"
    sudo -u www-data --preserve-env php /app/code/bin/console mautic:install \
        --db_host="${CLOUDRON_MYSQL_HOST}" \
        --db_port="${CLOUDRON_MYSQL_PORT}" \
        --db_name="${CLOUDRON_MYSQL_DATABASE}" \
        --db_user="${CLOUDRON_MYSQL_USERNAME}" \
        --db_password="${CLOUDRON_MYSQL_PASSWORD}" \
        --db_table_prefix= \
        --db_backup_tables=false \
        --admin_email=admin@cloudron.local \
        --admin_password="Changeme?1234" \
        -- "${CLOUDRON_APP_ORIGIN}"
     _generateconfig
else
    echo "==> Update config"
    _generateconfig
    echo "==> Migrating database"
    sudo -u www-data --preserve-env php /app/code/bin/console doctrine:migrations:migrate --no-interaction
fi

echo "==> Ensure plugins"
sudo -u www-data php /app/code/bin/console mautic:plugins:install # this populates the plugins table but not the plugin_integration_settings table

echo "==> Starting Mautic"
rm -f /run/apache2/apache2.pid
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Mautic


NEW_PASSWORD="Changeme?1234"
HASHED_PASSWORD=`php -r "echo password_hash(\"${NEW_PASSWORD}\", PASSWORD_BCRYPT);"`
mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "UPDATE users SET password=\"${HASHED_PASSWORD}\" WHERE username=\"admin\""

mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e 'UPDATE users SET password="admin@nebulon.de" WHERE username="admin"'
