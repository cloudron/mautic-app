## Overview

Mautic is an Open Source Marketing Automation Software.

Marketing automation has historically been a difficult tool to implement in a business. The Mautic community is a rich environment for you to learn from others and share your knowledge as well. Open source means more than open code. Open source is providing equality for all and a chance to improve. If you have questions then the Mautic community can help provide the answers.

Ready to get started with the community? You can get more involved on the Mautic website. Or follow Mautic on social media just to stay current with what's happening!

### Contact Info

* https://www.mautic.org
* [@MauticCommunity](https://twitter.com/MauticCommunity) [Twitter]
* [@MauticCommunity](https://www.facebook.com/MauticCommunity/) [Facebook]

## Bug reports

Open bugs on [GitHub](https://github.com/mautic/mautic)
