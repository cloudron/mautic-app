#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 20000;
    const LOCATION = process.env.LOCATION || 'test';
    const EXEC_ARGS = { env: process.env, cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    const adminUser = 'admin';
    const adminPassword = 'Changeme?1234';

    let app, browser;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        return await browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(() => browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function login(UsernameOrEmail, password) {
        await browser.manage().deleteAllCookies();
        await browser.get(`https://${app.fqdn}/s/login`);
        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(UsernameOrEmail);
        await browser.findElement(By.id('password')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(., "login") or contains(., "Login")]')).click();
        await browser.wait(until.urlIs(`https://${app.fqdn}/s/dashboard`), TEST_TIMEOUT);
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}/s/logout`);
        await browser.sleep(2000);
    }

    async function checkSiteUrl() {
        await browser.get(`https://${app.fqdn}/s/config/edit`);
        await waitForElement(By.xpath('//a[contains(., "System Settings")]'));
        await browser.findElement(By.xpath('//a[contains(., "System Settings")]')).click();
        await waitForElement(By.id('config_coreconfig_site_url'));
        const siteUrl = await browser.findElement(By.id('config_coreconfig_site_url')).getAttribute('value');
        console.log(`siteUrl: ${siteUrl}`);
        expect(`https://${app.fqdn}`).to.be(siteUrl);
    }

    async function addContact(firstname, lastname) {
        await browser.get(`https://${app.fqdn}/s/contacts/new`);
        await browser.findElement(By.id('lead_firstname')).sendKeys(firstname);
        await browser.findElement(By.id('lead_lastname')).sendKeys(lastname);
        await browser.findElement(By.id('lead_email')).sendKeys(`${firstname}_${lastname}@example.com`);
        await browser.findElement(By.id('lead_buttons_save_toolbar')).click();
        await browser.wait(until.urlContains('contacts/view'), TEST_TIMEOUT);
    }

    async function checkContact() {
        await browser.get(`https://${app.fqdn}/s/contacts`);
        await waitForElement(By.xpath('//div[contains(text(), "Max Mustermann")]'));
    }

    it('install app', () => execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS));

    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('has proper siteUrl', checkSiteUrl);
    it('can add contact', addContact.bind(null, 'Max', 'Mustermann'));
    it('can logout', logout);

    it('can restart app', () => execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS));
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('check contact', checkContact);
    it('can logout', logout);

    it('backup app', () => { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', () => {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);
    it('can login', login.bind(null, adminUser, adminPassword));
    it('has restored successfully', checkContact);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
    });
    it('can get app information', getAppInfo);

    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('has proper siteUrl', checkSiteUrl);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('install app (update)', () => execSync(`cloudron install --appstore-id org.mautic.cloudronapp --location ${LOCATION}`, EXEC_ARGS));
    it('can get app information', getAppInfo);
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('can add contact', addContact.bind(null, 'Max', 'Mustermann'));
    it('can logout', logout);

    it('can update', () => execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS));
    it('can get app information', getAppInfo);

    it('can change admin password', () => execSync(`cloudron exec -- set-admin-password ${adminPassword}`));
    it('can admin login', login.bind(null, adminUser, adminPassword));
    it('check contact', checkContact);
    it('can logout', logout);

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
