This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: Changeme?1234<br/>
**Email**: admin@cloudron.local<br/>

Please change the admin email and password credentials immediately.

<sso>
**IMPORTANT:** All users start as Mautic admins
</sso>
