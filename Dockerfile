FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=mautic/mautic versioning=semver
ARG MAUTIC_VERSION=5.2.3

RUN cd /tmp \
  && curl -sSL https://github.com/mautic/mautic/releases/download/$MAUTIC_VERSION/$MAUTIC_VERSION.zip -o ./mautic.zip \
  && unzip -q ./mautic.zip -d /app/code \
  && rm ./mautic.zip

# https://symfony.com/doc/current/mailer.html (versions match symfony - https://github.com/mautic/mautic/blob/5.x/app/composer.json#L27)
RUN composer require --prefer-stable --no-scripts symfony/amazon-mailer:^5.4 symfony/mailgun-mailer:^5.4 symfony/postmark-mailer:^5.4 symfony/sendgrid-mailer:^5.4 symfony/mailjet-mailer:^5.4

# make plugins, themes writeable, start.sh will copy them initially
RUN mv /app/code/plugins /app/code/plugins.orig && ln -s /app/data/plugins /app/code/plugins \
  && mv /app/code/themes /app/code/themes.orig && ln -s /app/data/themes /app/code/themes \
  && mv /app/code/var /app/pkg/var && ln -s /run/mautic/var /app/code/var \
  && rm -rf /app/code/config && ln -s /app/data/config /app/code/config \
  && rm -rf /app/code/translations && ln -s /app/data/translations /app/code/translations \
  && mv /app/code/media/images /app/code/media/images.orig && ln -s /app/data/media/images /app/code/media/images \
  && mv /app/code/media/files /app/code/media/files.orig && ln -s /app/data/media/files /app/code/media/files

# configure apache
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 8000" > /etc/apache2/ports.conf && \
    a2enmod rewrite && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    ln -sf /app/data/apache/mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf

ADD apache/mautic.conf /etc/apache2/sites-enabled/mautic.conf

# Memory limit comes from https://github.com/mautic/mautic/issues/8932
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 25M \
    && crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 25M \
    && crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 512M \
    && crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/mautic/sessions \
    && crudini --set /etc/php/8.1/apache2/php.ini Date date.timezone Europe/Berlin

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# configure cron . clean out existing
RUN rm -rf /var/spool/cron && ln -s /run/cron /var/spool/cron \
    && rm -f /etc/cron.d/* /etc/cron.daily/* /etc/cron.hourly/* /etc/cron.monthly/* /etc/cron.weekly/* \
    && truncate -s0 /etc/crontab

# configure supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY set-admin-email set-admin-password /usr/bin/
COPY apache/mpm_prefork.conf crontab.system.template start.sh /app/pkg/

CMD ["/app/pkg/start.sh"]
