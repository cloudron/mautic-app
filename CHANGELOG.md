[1.0.0]
* Initial version

[1.0.1]
* Follow pre-setup admin account password convention
* Add documentation url

[1.0.2]
* Update to Mautic 2.15.2

[1.0.3]
* Update Mautic to 2.15.3

[1.0.4]
* Fix mailer_encryption setting

[1.1.0]
* Update Mautic to 2.16.0

[1.1.1]
* Update Mautic to 2.16.1

[1.1.2]
* Update Mautic to 2.16.2

[1.2.0]
* Use latest base image 2.0.0

[1.3.0]
* Run apache on less previliged port 8000

[1.3.1]
* Update Mautic to 2.16.3
* [Full changelog](https://github.com/mautic/mautic/releases/tag/2.16.3)
* This prepares for Mautic 3.x migration

[2.0.0]
* Update Mautic to 3.1.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.1.0)
* [Mautic 3: Building stable foundations](https://www.mautic.org/blog/community/mautic-3-building-stable-foundations)
* Add support for Amazon SES API (@renjith341, @chocopowwwa, @idealaunchtech, @Wise-Gorilla & @mabumusa1)
* Add company view (@alfredoct96)
* Reply API endpoint (@escopecz)
* Add the Pepipost SMTP service (@moskoweb)
* Add the Psysh REPL for dev usage (@dongilbert)

[2.0.1]
* Update Mautic to 3.1.1
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.1.1)
* #9121 Fix broken link in the readme (@RCheesley)
* #9229 Fix handling of empty reasoncode in DNC API (@dennisameling)
* #9226 Fix doNotContact on contacts created through API (@dennisameling)
* #9215 Add DNS check (@RCheesley) based on #9144 (@evgu)
* #9176 Update ListType.php to show all categories in list showing them (segments, preference center) (@npracht)
* #9170 Fix migration template (@kuzmany)
* #9168 Fix notification style (@kuzmany)
* #9143 Fix JS issue for code mode (@kuzmany)
* #9108 handle urls with unicode characters (@mabumusa1)
* #9062 Sanitizing select aliases during generating report query (@escopecz)
* #8830 Fixes Anchor in url doesn’t work after redirect with utm (@directpromo)
* #8785 Hubspot support for read only fields (@kuzmany)
* #8228 Return unique clicked stats in email detail stats graph (@kuzmany)
* #7956 Add missing dist sections (@flossels)
* #7816 Fix message_queue job loops and incorrect schedule dates (@anton-vlasenko)

[2.0.2]
* Update Mautic to 3.1.2
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.1.2)
* #9303 replace getName() to getBlockPrefix() (@pety-dc)
* #9302 Fix unsubscribe from contact preference page (@kuzmany)
* #9301 Add clonedId to email entity (@kuzmany)
* #9300 Append Tracking to body of emails. (@mabumusa1)
* #9295 Fix error dynamic content without variants (@kuzmany)
* #9293 Fix cache key contains reserved characters with API limiter (3.1 re-PR) (@dennisameling)
* #9289 Focus preview even If website fetch url doesn't exist (@kuzmany)
* #9277 Change lead company link to View instead of Edit (@colourfulowl)
* #9276 Fix failed send mail to contact campaign action when CC or BCC are incorrectly formatted (@simcen)
* #9274 Fix not loaded core integrations like Twilio/OneSignal in Windows (@kuzmany)
* #9269 Remove font-size:0 (@moskoweb)
* #9266 Update Travis to Bionic 18.04 (@dennisameling)
* #9261 Fix remove DNC from contact profile (@kuzmany)

[2.1.0]
* Update Mautic to 3.2.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.2.0)
* Adding a new boolean option system setting to transliterate
* #9422 Add "Export to Excel" functionality for contacts (@dennisameling)
* #9387 Functional test performance improvement (@fedys)
* #9371 Enabling company tokens in webhook campaign action (@mabumusa1)
* #9367 Add thumbnails to file manager (@kuzmany)
* #9344 Email attachment tooltip with info about tracking added (@escopecz)
* #9342 Adding missing campaign action add/remove DNC description (@escopecz)

[2.1.1]
* Update Mautic to 3.2.1
* This is a security hotfix release to resolve a vulnerability reported in ELFinder present in all versions of Mautic from 3.0.

[2.1.2]
* Update Mautic to 3.2.4
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.2.4)
* Security hotfix release
* [Important announcement](https://www.mautic.org/blog/community/security-release-all-versions-mautic-prior-2-16-5-and-3-2-4)

[2.1.3]
* Update Mautic to 3.2.5
* #9594 Show warning on dashboard if user is using unsupported PHP version (@dennisameling)
* #9522 Update PHPSTAN to 0.12.25 (@dennisameling)
* #9520 Fix dashboard query performance (@hluchas)
* #9519 Focus items in dynamic web content (@hluchas)
* #9515 Fix absolute url in pages (@kuzmany)
* #9476 Fix gated video showing play button above form (Issue #9431) (@gabepri)
* #9458 Update db_server_version on existing Mautic instances (@dennisameling)
* #9410 Check for country/city presence before use. (@letharion)
* #8746 Fix: Report when use Contact Point Log as data source and filter by Segment (@dev-marcel)
* #8605 Fix input format for onesignal. (@crasx)
* #7815 Fixed batch delete response inconsistencies (@alanhartless)

[2.1.4]
* Update to new base image v3

[2.2.0]
* Update Mautic to 3.3.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.3.0)
* add mjml theme paprika #9681
* add mjml theme confirm me #9682
* add mjml theme brienz #9683
* Add grape js builder bundle. #9677
* Support for builder specific templates #9654
* Add DDEV first-run experience rocket #9669
* Add segment filter info message #9610
* Webhook processing time limit #9573
* Adding new templating helper as a proxy for CoreParametersHelper #9570

[2.2.1]
* Update Mautic to 3.3.1
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.3.1)
* Include GrapesJsBuilderBundle in build script for update package #9713
* No GrapeJS builder after 3.3.0 upgrade #9710
* No landing page theme available after enabling GrapesJS builder #9715

[2.3.0]
* Make email setup optional.

[2.3.1]
* Update Mautic to 3.3.2
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.3.2)
* [Security issue](https://github.com/mautic/mautic/security/advisories/GHSA-4hjq-422q-4vpx)
* Fix company change audit log after modify contact #9770
* Reduce ajax call for email count stats #9712
* Search with wildcards #9679
* Fix load filemanager from builder #9651
* Bug lead graph report with company filters #9560
* Remove duplicate index #9510
* added css to center contact name in search result #9438 #4758

[2.3.2]
* Update Mautic to 3.3.3
* [Full changelog](https://github.com/mautic/mautic/releases/tag/3.3.3)
* Fix audit log for few entities #9801
* Fix contact CSV import progress checks unreachable statement #9782
* Changes company links to action /view in reports #9768
* Fix date time field display issue #9747
* Fix report asset downloads counts #9641
* Fix Salesforce sync boolean type value #9618

[2.3.3]
* Update apache config to log the client IP correctly

[2.3.4]
* Update Mautic to 3.3.4
* CVE-2021-27913 Use of a Broken or Risky Cryptographic Algorithm
* CVE-2021-27912 XSS vulnerability on asset view
* CVE-2021-27911 XSS vulnerability on contacts view
* CVE-2021-27910 Stored XSS vulnerability on Bounce Management Callback
* CVE-2021-27909 XSS vulnerability on password reset page

[2.4.0]
* Update Mautic to 4.0.0
* [Release announcement](https://www.mautic.org/blog/community/mautic-4-standing-tall)
* If you use plugins, please make sure they are compatible with Mautic 4 before upgrading
* New email and landing page builder based on the open source GrapesJS framework
* Four new MJML email templates
* An interface for managing tags within Mautic
* A read-only first phase of the new Mautic Marketplace
* An updated Zapier app including support for OAuth2

[2.4.1]
* Update Mautic to 4.0.2
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.0.2)

[2.5.0]
* Update Mautic to 4.1.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.1.0)

[3.0.0]
* Update Mautic to 4
* [Release announcement](https://www.mautic.org/blog/community/mautic-4-standing-tall)
* If you use plugins, please make sure they are compatible with Mautic 4 before upgrading
* Move any custom cron jobs from crontab.user to Cloudron's per app cron - https://docs.cloudron.io/apps/mautic/#custom-cron-jobs
* New email and landing page builder based on the open source GrapesJS framework
* Four new MJML email templates
* An interface for managing tags within Mautic
* A read-only first phase of the new Mautic Marketplace
* An updated Zapier app including support for OAuth2

[3.0.1]
* Update Mautic to 4.1.2
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.1.2)
* Fix lock wait deleting unused ips by @ts-navghane in #10710
* Fix contact batch api for single id value by @rahuld-dev in #10700
* fix(LeadTimelineEvent): keys do not match the func args, strips keys by @abailey-dev in #10663
* User update role api by @ts-navghane in #10668
* Fix send unpublished SMS by @kuzmany in #10577
* Fix group by If you use count columns for assets download by @kuzmany in #10693
* Fixing background color contrast for buttons in the builder by @eloimarquessilva in #10728
* Ensure default frequency rules are taken into account correclty by @mollux in #10753
* Change field definition for column_value to longtext instead of varchar by @escopecz in #10778
* Correct bug no forms in the select contact source campaign when the forms have identical names by @tomekkowalczyk in #10717
* Restore X value in contact boolean fields by @patrykgruszka in #10716
* Fix for pushing to integrations in campaign actions by @TonyBogdanov in #10674

[3.1.0]
* Update Mautic to 4.2.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.2.0)

[3.1.1]
* Update Mautic to 4.2.1
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.2.1)
* CVE-2022-25769 - Improper regex in htaccess file
* Support for emoji in email subjects by @ts-navghane in #10844
* Change btn-primary class of email buttons in campaign send email action to btn-default by @volha-pivavarchyk in #10837
* Add description on Tag form by @mzagmajster in #10799
* Add dynamic content to the Email Builder in MJML mode by @adiux in #10782
* Segment fail notifications by @escopecz in #10772

[3.1.2]
* Update Mautic to 4.2.2
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.2.2)
* Add a way to directly modify the html code in Advanced mode by @volha-pivavarchyk in https://github.com/mautic/mautic/pull/11102
* Many bugfixes

[3.2.0]
* Update Mautic to 4.3.1
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.3.0)
* CVE-2021-27914 - XSS vulnerability in installer
* CVE-2022-25772 - XSS vulnerability in tracking pixel functionality

[3.3.0]
* Update Mautic to 4.4.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.0)
* Add default parameters to public method by @kuzmany in #11259
* Email address with two dots mark as invalid by @kuzmany in #11258
* Disable default values by @escopecz in #11250
* Do not perform delete queries on the sync tables for anonymous contacts by @escopecz in #11262
* Report date fix (mostly added test) by @escopecz in #11256
* Do not retry all objects if one of them should be retried. Retry only the right one(s) by @escopecz in #11252
* Disable default values [for fields used to identify the contact] by @escopecz in #11250
* Fix normalize value for boolean contact field by @kuzmany in #11235
* Flip array returned by getDateChoices by @pety-dc in #11233
* Code mode fix by @escopecz in #11221
* Tag import fix by @escopecz in #11220
* Align lock file with change from #11203 by @mollux in #11218
* Ensure the app folder is seen as the 4.3.x instead of a specific version in composer by @mollux in #11213
* Improve GrapesJS dev demo by @adiux in #11210
* Fixing DoctrineEventSubscriber for plugin installations by @escopecz in #11206
* Fix: The decision step ignores the preceding condition in the campaign by @patrykgruszka in #11200
* Marketplace version fix by @escopecz in #11197
* The email update success message is a notice (by default) by @volha-pivavarchyk in #11192
* Integer range point action validation by @aarohiprasad in #11191
* Fix the issue with saving contact settings by @volha-pivavarchyk in #11187
* Modify the form label by @volha-pivavarchyk in #11166
* Email dc multi select filter by @shinde-rahul in #10871

[3.3.1]
* Update Mautic to 4.4.1
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.1)
* Add translations for new blocks by @volha-pivavarchyk in #11006
* Fix for all foreign tables segment filter with empty/notEmpty expression by @kuzmany in #11253
* change lead to company trans key by @npracht in #11300
* Pending query optimization by @escopecz in #11260
* Check existing property (fixes #11299) by @KN4CK3R in #11321
* Fix user language preference over system language by @ts-navghane in #11119
* Segment lookup_id field : use the data-action attribute if present by @benji07 in #11327
* Fix report export to Excel with aggregated bool columns by @patrykgruszka in #11298
* Changes from PR#10782 rebuilt by @annamunk in #11319
* Fix No Data shown for Most hit email redirects dashboard widget by @ts-navghane in #11086
* Fix special characters in form condition value by @kuzmany in #11093
* Cancelling new campaign should not give 500 error. by @biozshock in #11348
* Fixing Composer install/require by @escopecz in #11353
* Fix import errors when Don't override value is enabled by @biozshock in #11350
* Fix issue 11267 - Sending emails via API should respect useOwnerAsMailer by @abcpro1 in #11347
* Use proper env variable processor for rememberme_lifetime. by @biozshock in #11363

[3.3.2]
* Update Mautic to 4.4.2
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.2)
* Do not embed images before the email is really sent. by @biozshock in #11362
* Flip flipped locales. by @biozshock in #11364
* Hide tooltips on keydown and wait for a user to stop typing. by @biozshock in #11383
* Skip embedding a tracking pixel. by @biozshock in #11390
* Fix: Email builders code editor allows to save after closing and reopening by @adiux in #11376
* Implementing the RemoveUnusedPrivateMethodParameterRector Rector rule by @escopecz in #11232
* Include into links tracking. by @biozshock in #11391
* Fixing PHPSTAN issue with PHP8 on 4.4 branch by @escopecz in #11394
* Fix property must not be accessed before initialization error by @ts-navghane in #11388
* New segment test case by @escopecz in #11404
* Fix gitpod for form submissions by @RCheesley in #11409
* chore: Set permissions for GitHub actions by @neilnaveen in #11295
* Copy to CC or BCC should send an email even if TO field is empty. by @biozshock in #11405
* Show form validation errors if an integration is published or if the … by @shinde-rahul in #10539
* Avoid api cache clear by @escopecz in #11420
* Reverting PR 11353 that used wrong branch by @escopecz in #11373
* Fix MJML issues with Brienz template by @RCheesley in #11356

[3.3.3]
* Update Mautic to 4.4.3
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.3)
* Add explicit order by id when as dependant fields have same field order by @rohitpavaskar in #11437
* Fix issue searching company on contact edit page when more than 100 companies in DB (issue #11455) by @pjcarly in #11457
* Hide category list from form download action if you do not need it by @kuzmany in #11461
* Fix download asset from form action by @kuzmany in #11462
* Test fixes from 4.4.2 to 5.x merge by @escopecz in #11476
* Fix gated video with froogaloop library by @kuzmany in #11467
* Fix best hours widget with segment filters by @kuzmany in #11485
* Focus items fix unique clicks link in focus view by @AlanWierzchonCA in #11418
* Fix a JS failure when the show pause preferences setting is disabled by @volha-pivavarchyk in #11431
* CSRF errors in AJAX endpoints by @shinde-rahul in #10567
* Ensure there is a default DB prefix during PHPUnit tests (4.4) by @mollux in #11496
* Fix Dynamic Content access control by @AlanWierzchonCA in #11278
* Fix missing MAUTIC_TABLE_PREFIX in isolated tests by @mollux in #11507

[3.3.4]
* Update Mautic to 4.4.4
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.4)
* Hubspot - Fix errors when the API returns a 401 code by @Cr7t3K in #11416
* Fix created by and modified date for clone entity by @kuzmany in #11528
* Fix import Company error message without unique fields by @kuzmany in #11512
* Change clean conditional values with InputHelper::clean by @kuzmany in #11513
* Add test for UI installer to ensure basic config works (4.4) by @mollux in #11559
* Segment - Fix filter URL Visited with regex and more by @Cr7t3K in #11541
* Update SpoolTransport.php to set new MessageID for each email by @pwned555 in #11400
* Fix incorrect --quiet flag in ProcessEmailQueueCommand (4.4) by @mollux in #11571
* Fix Campaign events reschedules due to DST by @rohitpavaskar in #11602
* Dynamic Content filters fixed by @annamunk in #11425
* Fix day graph range by @kuzmany in #11544

[3.3.5]
* Update Mautic to 4.4.5
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.5)
* fixed starting mandrill transport in immediately mode by @Rigiytip in #11607
* Fix ampersand encoding in tokens - backport to 4.4 by @JaZo in #11676
* Fix show Focus Items Builder - correction of reading sending data by @AlanWierzchonCA in #11687
* Fix infinite recursion in search string helper - backport for 4.4 by @Rigiytip in #11714
* Fixing Custom Objects plugin - Upsert functionality 4.4 by @aarohiprasad in #11746
* Resolving Hubspot Mapping Error by @onairmarc in #11753
* Update Transifex to use new SDK & improve code for M4 by @escopecz in #11758
* Fix/change hubspot api auth by @npracht in #11470

[3.3.6]
* Update Mautic to 4.4.6
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.6)
* Fix marketing message sorting by category (v4.x) by @volha-pivavarchyk in #11733
* Fix html code with unicode content by @kuzmany in #9516
* Add orderBy to findOneBy to return more accurate eventLog result by @pwned555 in #11636
* Clarify global segment setting's tooltip by @bradjones1 in #11809

[3.3.7]
* Update Mautic to 4.4.7
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.7)
* Fix date/datetime empty mysql expression for contact field condition by @kuzmany in #11660
* Fix: Dynamic Content in emails (legacy builder)- default content is not saved properly by @irfanhanfi in #11887
* Fix failing API test on PHP 8 by @mollux in #11957
* Fix error when /tmp is not available by @pedrodejesus in #9929
* Fix automated test for M4 branch by @escopecz in #12069
* Custom object export fix by @escopecz in #11989
* Fix resubscribe action condition check by @mabumusa1 in #11918

[3.3.8]
* Update Mautic to 4.4.8
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.8)
* Improve test speed of campaign count tests (backport 4.4) by @mollux in https://github.com/mautic/mautic/pull/12158
* Fix segment mails not using site_url [backport to 4.x] by @nick-vanpraet in https://github.com/mautic/mautic/pull/11920
* Get request variables from query instead of request. by @irfan-synerzip in https://github.com/mautic/mautic/pull/12002
* fix for email graph stats when switching between variant and all by @mollux in https://github.com/mautic/mautic/pull/12050
* Bugfix 12063 - 500 Error when changing tag in a Contact and clicking save&close [4.x] by @LordRembo in https://github.com/mautic/mautic/pull/12105
* Add exit code to some mautic commands by @beetofly in https://github.com/mautic/mautic/pull/11963

[3.3.9]
* Update Mautic to 4.4.9
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.9)
* Deprecating Pipedrive by @escopecz in #12364 (read more in this blog post about alternatives)
* Adding Citrix plugin deprecation for M4 so it could be removed in M5 by @mabumusa1 in #12367 (read more in this article about alternatives)

[3.3.10]
* Update Mautic to 4.4.10
* [Full changelog](https://github.com/mautic/mautic/releases/tag/4.4.10)
* AJAX requests use the site_url instead of the current domain [backport] by @nick-vanpraet in #12042
* ensure the elfinder paths are within the default firewall, so rememberme functionality works (backport) by @mollux in #12433
* ensure dependencies use a compatible version of psr/simple-cache - 4.4 by @mollux in #12453
* Fixing point translation (M4) by @escopecz in #12499
* ensure only leads from a company are shown on a the company detail overview - 4.4 by @mollux in #12466
* re-add missing commit to fix incorrect reporting dates by @mollux in #12541
* Update analytics.js to gtag to support ga4 by @martoboto in #12525
* Switching contact import to finish in background causes 500 by @nick-vanpraet in #12538
* Fixes #11888 for mailjet puts only : in lead_donotcontact.comment by @beetofly in #11912
* Fix: Integration Campaign Members List issue by @irfanhanfi in #12058
* add codecov token to reduce the number of failed tests due to report upload failures - 4.4 by @mollux in #12610
* Fix Dynamic content block in email behaving differently in first save and edit [4.4] by @danadelion in #12634

[3.4.0]
* Copy over media assets

[3.5.0]
* Add support for aliases

[4.0.0]
* Update Mautic to 5.0.2
* [Release announcement](https://www.mautic.org/blog/community/mautic-5-beyond-expectations-beyond-limits)
* **Important Upgrade Notes:**
  * This is a major upgrade to Mautic. Please raed the changelog thoroughly before upgrading
  * There are breakages to the plugin API and many plugins don't work anymore
  * The Cloudron LDAP integration is disabled now because the LDAP plugin doesn't work anymore
  * Post update, you can login as admin and reset the password of the previous LDAP users

[4.0.1]
* Update Mautic to 5.0.3
* [Full changelog](https://github.com/mautic/mautic/releases/tag/5.0.3)
* Focus first invalid element of form on validation error by @kuzmany in #13247
* Update preview form script source path for dev enviroment by @kuzmany in #13248
* Fix replace entity with reference for detached lists for add to segment action by @kuzmany in #13244
* Fix incorrect twig field checks for contacts and companies by @mollux in #13254
* fix incorrect version of the SpBundle dependency by @mollux in #13253
* Fix call to logger warn method. by @AdamBark in #13252
* Fix incorrect occurrences of dns where it should be dsn by @mollux in #13259
* Fix using messenger DSN with special characters by @mollux in #13255
* fix incorrect migration from old mail config to mailer_dsn config by @mollux in #13256
* add missing translations for flash messages by @mollux in #13258

[4.0.2]
* Use a more up to date composer

[4.0.3]
* Install mailer transports `symfony/amazon-mailer` `symfony/mailgun-mailer` by default

[4.1.0]
* Fix installation of mailer transports
* Use `mailer_dsn` to configure email sending
* Add support for display name

[4.1.1]
* Update Mautic to 5.0.4
* [Full changelog](https://github.com/mautic/mautic/releases/tag/5.0.4)
* CVE-2021-27915 - XSS Cross-site Scripting Stored in Description field - GHSA-2rc5-2755-v422
* CVE-2022-25774 - XSS in Notifications via saving Dashboards - GHSA-fhcx-f7jg-jx3f
* CVE-2021-27916 - Relative Path Traversal / Arbitrary File Deletion in GrapesJS builder - GHSA-9fcx-cv56-w58p
* CVE-2022-25775 - SQL Injection in dynamic Reports - GHSA-jj6w-2cqg-7p94
* CVE-2022-25776 - Sensitive Data Exposure due to inadequate user permission settings - GHSA-qjx3-2g35-6hv8
* CVE-2022-25777 - Server-Side Request Forgery in Asset section - GHSA-mgv8-w49f-822w
* DPMMA-2401 Use object's timezone when comparing with 'now' in DateTimeHelper by @patrykgruszka in #13320
* Fix form api create without post action parameter by @kuzmany in #13410
* DPMMA-2462 Fix Autowiring Dependency for PushToIntegrationTrait by @patrykgruszka in #13470
* DPMMA-2600 Fix for Grapesjs-Mjml self-closing tag issue by @patrykgruszka in #13431
* The API defines Contacts not Contact causing the API to not receive the correct mapping by @mallezie in #13208

[4.2.0]
* make apache mpm prefork configurable

[4.3.0]
* Update Mautic to 5.1.0
* [Full changelog](https://github.com/mautic/mautic/releases/tag/5.1.0)

[4.3.1]
* Fix issue where cron runtime dir was not created

[4.3.2]
* Update Mautic to 5.1.1
* [Full changelog](https://github.com/mautic/mautic/releases/tag/5.1.1)
* CVE-2022-25768 - Improper access control in UI upgrade process - Reported by @mollux, fixed by @mollux and tested/reviewed by @escopecz and @patrykgruszka in GHSA-x3jx-5w6m-q2fc.
* CVE-2024-47058 - Cross-site Scripting (XSS) - stored (edit form) - reported by @MatisAct, fixed by @lenonleite and tested/reviewed by @escopecz and @avikarshasha in GHSA-xv68-rrmw-9xwf.
* CVE-2024-47050 - Cross-site Scripting (XSS) in contact/company tracking - reported by @mqrtin, fixed by @patrykgruszka and tested/reviewed by @escopecz in GHSA-73gr-32wg-qhh7.
* CVE-2021-27917 - Cross-site Scripting (XSS) in contact tracking and page hits report - reported by @patrykgruszka, fixed by @lenonleite and tested/reviewed by @escopecz and @lenonleite in GHSA-xpc5-rr39-v8v2.
* CVE-2024-47059 - User enumeration through weak password login prompt - reported and fixed by @tomekkowalczyk and tested/reviewed by @escopecz and @patrykgruszka in GHSA-8vff-35qm-qjvv.
* CVE-2022-25770 - Removal of upgrade.php file which can have insufficient authentication - reported and fixed by @mollux, tested/reviewed by @kuzmany, @escopecz and @patrykgruzska in GHSA-qf6m-6m4g-rmrc.
[4.4.0]
* Update mautic to 5.2.0
* [Full Changelog](https://github.com/mautic/mautic/releases/tag/5.2.0)
* Optimizing contacts activity API (refactoring of MR-10237 for Mautic v5) by [@&#8203;Moongazer](https://github.com/Moongazer) in https://github.com/mautic/mautic/pull/12305
* Refactor DBAL execute method to executeQuery. by [@&#8203;biozshock](https://github.com/biozshock) in https://github.com/mautic/mautic/pull/14139
* Using "anonymous: lazy" to make the firewall lazy is deprecated, use "anonymous: true" and "lazy: true" instead. by [@&#8203;biozshock](https://github.com/biozshock) in https://github.com/mautic/mautic/pull/14124
* The "security.encoder_factory.generic" service is deprecated, use "scurity.password_hasher_factory" instead. by [@&#8203;biozshock](https://github.com/biozshock) in https://github.com/mautic/mautic/pull/14125
* \[UI] Refactor hardcoded buttons using Twig template by [@&#8203;andersonjeccel](https://github.com/andersonjeccel) in https://github.com/mautic/mautic/pull/14233
* \[UX] Updating Blank theme to MJML by [@&#8203;andersonjeccel](https://github.com/andersonjeccel) in https://github.com/mautic/mautic/pull/14255
* Referencing controllers with a single colon is deprecated. by [@&#8203;biozshock](https://github.com/biozshock) in https://github.com/mautic/mautic/pull/14130
* Update readme and devdocs link by [@&#8203;laurielim](https://github.com/laurielim) in https://github.com/mautic/mautic/pull/14207

[4.4.1]
* Update mautic to 5.2.1
* [Full Changelog](https://github.com/mautic/mautic/releases/tag/5.2.1)
* \[UI/UX] Search (almost) Everything by [@&#8203;andersonjeccel](https://github.com/andersonjeccel) in https://github.com/mautic/mautic/pull/14353
* Add support to check duplicates for api/companies/batch/new by [@&#8203;kuzmany](https://github.com/kuzmany) in https://github.com/mautic/mautic/pull/12273
* fix: \[DPMMA-2945] use hex colors in ckeditor by [@&#8203;patrykgruszka](https://github.com/patrykgruszka) in https://github.com/mautic/mautic/pull/14322
* fix: delete emails deleting contacts by [@&#8203;andersonjeccel](https://github.com/andersonjeccel) in https://github.com/mautic/mautic/pull/14335
* fix: theme upload width by [@&#8203;andersonjeccel](https://github.com/andersonjeccel) in https://github.com/mautic/mautic/pull/14334

[4.4.2]
* Update mautic to 5.2.2
* [Full Changelog](https://github.com/mautic/mautic/releases/tag/5.2.2)
* Add missing "isIndexed" and "charLegthLimit" fields to the API response of Contact Fields. by [@&#8203;biozshock](https://github.com/biozshock) in https://github.com/mautic/mautic/pull/14442
* fix: Creating or updating a contact via the Rest API discards seconds for date time fields by [@&#8203;driskell](https://github.com/driskell) in https://github.com/mautic/mautic/pull/14484
* Fix FormSubscriberTest by [@&#8203;fedys](https://github.com/fedys) in https://github.com/mautic/mautic/pull/14474
* Update decision/action panel colors in campaign's builder by [@&#8203;Hugo-Prossaird](https://github.com/Hugo-Prossaird) in https://github.com/mautic/mautic/pull/14404
* Fix template for Campaign Editor by [@&#8203;bastolen](https://github.com/bastolen) in https://github.com/mautic/mautic/pull/14491
* DPMMA-3048 Fix campaign execution stuck due to incorrect lead detachment in membership change action by [@&#8203;patrykgruszka](https://github.com/patrykgruszka) in https://github.com/mautic/mautic/pull/14497
* Add allowed protocols for links in CK5, so people can add phone links by [@&#8203;LordRembo](https://github.com/LordRembo) in ht
* ...

[4.4.3]
* Update mautic to 5.2.3
* [Full Changelog](https://github.com/mautic/mautic/releases/tag/5.2.3)
* CVE-2024-47053 - Improper Authorization in Reporting API - Reported by [@&#8203;putzwasser](https://github.com/putzwasser), fixed by [@&#8203;lenonleite](https://github.com/lenonleite) and tested/reviwed by [@&#8203;escopecz](https://github.com/escopecz) and [@&#8203;patrykgruszka](https://github.com/patrykgruszka) in https://github.com/mautic/mautic/security/advisories/GHSA-8xv7-g2q3-fqgc
* CVE-2022-25773 - Relative Path Traversal in assets file upload - Reported by [@&#8203;majkelstick](https://github.com/majkelstick) and [@&#8203;patrykgruszka](https://github.com/patrykgruszka), fixed by [@&#8203;patrykgruszka](https://github.com/patrykgruszka) and tested/reviewed by [@&#8203;escopecz](https://github.com/escopecz) and [@&#8203;lenonleite](https://github.com/lenonleite) in https://github.com/mautic/mautic/security/advisories/GHSA-4w2w-36vm-c8hf
* CVE-2024-47051 - Remote Code Execution & File Deletion in Asset Uploads - Reported by [@&#8203;mallo-m](https://github.com/mallo-m), fixed by [@&#8203;lenonleite](https://github.com/lenonleite) and tested/reviewed by [@&#8203;patrykgruszka](https://github.com/patrykgruszka) in https://github.com/mautic/mautic/security/advisories/GHSA-73gx-x7r9-77x2
* DPMMA-3031 Configurable email address length limit to prevent delivery issues by [@&#8203;patrykgruszka](https://github.com/patrykgruszka) in https://github.com/mautic/mautic/pull/14577
* Fixing the audit log widget when a contact is deleted by [@&#8203;escopecz](https://github.com/escopecz) in https://github.com/mautic/mautic/pull/14541
* Fixing segment building with default timezone by [@&#8203;escopecz](https://github.com/escopecz) in https://github.com/mautic/mautic/pull/14549
* Email click tracking fix, PHP warning fix by [@&#8203;escopecz](https://github.com/escopecz) in https://github.com/mautic/mautic/pull/14540
* fix: Fix font selection in CKEditor not including fallback fonts in output by [@&#8203;driskell](https://github.com/driskell) in https://github.com/mautic/mautic/pull/14539

